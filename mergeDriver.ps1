param (
    [Parameter(Mandatory=$true)][string]$baseFile,
    [Parameter(Mandatory=$true)][string]$localFile,
    [Parameter(Mandatory=$true)][string]$remoteFile
)

$PSDefaultParameterValues['*:Encoding'] = 'utf8'

((Get-Content $localFile) -join "`n") + "`n" | Set-Content -NoNewline $localFile

exit 0
